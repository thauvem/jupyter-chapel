from setuptools import setup

setup(name='jupyter_chapel_kernel',
      version='0.0.1',
      description='Minimalistic Chapel kernel for Jupyter, essentially a fork of the C kernel',
      author='Matthew Thauvette',
      url='https://gitlab.com/thauvem/jupyter-chapel/',
      download_url='https://gitlab.com/thauvem/jupyter-chapel/tarball/0.0.1',
      packages=['jupyter_chapel_kernel'],
      scripts=['jupyter_chapel_kernel/install_chapel_kernel'],
      keywords=['jupyter', 'notebook', 'kernel', 'chapel']
      )
