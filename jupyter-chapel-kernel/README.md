# A Jupyter kernel for Cray's Chapel language.

When it works, this is what it says on the tin - a minimal Jupyter kernel for Cray's chapel language. It was written as a fork of Brendan Rius' C kernel for jupyter.
Note that it's still fairly rough around the edges - some of that may be ironed out over the next week or two, but chances are it'll mostly stay unless an intrepid soul makes a new repo obsoleting this one.

## Manual installation

Tested only on Linux. You could attempt to run it with Docker similarly to the Jupyter C kernel and it might work, but I haven't tried.

 * Make sure you have the following requirements installed:
  * Chapel - at the moment the program assumes that the 'chpl' compiler is in your $PATH, so make sure it is.
  * jupyter
  * python 3
  * pip

### Step-by-step:
 * `pip install jupyter-chapel-kernel`
 * `install_chapel_kernel`
 * `jupyter-notebook`
 
 If it doesn't work, you probably know as much as I do what the issue is. Good luck!
 
## License
[MIT](LICENSE.txt)

## TODO

* Add multi-cell support? (perhaps similar to Elm workaround) // POSTPONED